package com.volidar.blitznote.client.android

import com.volidar.blitznote.client.android.common.{DialogUtils, SendEmailExceptionHandler, BaseActivity}
import android.os.{Parcelable, Bundle}
import com.volidar.blitznote.client.android.settings.SettingsActivity
import com.volidar.blitznote.client.android.data.EmailConfig
import android.media.MediaPlayer
import scala.concurrent.Future
import scala.collection.JavaConversions._
import android.content.Intent
import android.graphics.{Canvas, Bitmap}
import com.volidar.blitznote.client.android.data.base.{createMatrix, ZSize}
import java.io.ByteArrayOutputStream
import com.volidar.blitznote.client.android.EmailHelper.Attachment
import android.net.Uri
import scala.sys.process.BasicIO

class ShareViaActivity extends BaseActivity(R.layout.activity_share) {
   private lazy val preferences = MainActivity.getMyPreferences(this)

   protected override def onCreate(savedInstanceState: Bundle): Unit = {
      super.onCreate(savedInstanceState)
      SendEmailExceptionHandler.applyToCurrentThread()
   }

   protected override def onResume() {
      super.onResume()
      sendEmail()
   }

   private def sendEmail() {
      //TODO code duplicate from MainAction
      EmailConfig(preferences.getString(MainActivity.emailConfigStateKey, "")) match {
         case Some(emailConfig) => sendEmail(emailConfig)
         case None =>
            val emailSettings = ActivityRequest(classOf[SettingsActivity], classOf[Option[EmailConfig]]) {
               case None => DialogUtils.toast(this, "Configuration has been cancelled!")
               case Some(emailConfig) =>
                  val preferencesTx = preferences.edit()
                  preferencesTx.putString(MainActivity.emailConfigStateKey, emailConfig.asString)
                  preferencesTx.commit()
                  //sendEmail(emailConfig) //not needed because when we come back from Settings, we resume - and sendEmail there.
            }
            startActivityForResponse(emailSettings, None)
      }
   }

   private def sendEmail(emailConfig: EmailConfig) {
      DialogUtils.toast(this, s"Sending email to ${emailConfig.targetEmail}")

      val intent = getIntent
      val mimeType = intent.getType
      val extras = intent.getExtras
      val extrasMap = extras.keySet().map(key => key -> String.valueOf(extras.get(key))).toMap

      val subjectOpt: Option[String] = if(extras.containsKey(Intent.EXTRA_SUBJECT)) Some(extras.getString(Intent.EXTRA_SUBJECT)) else None
      val texts: List[String] = mimeType match {
         case "text/plain" => intent.getAction match {
            case Intent.ACTION_SEND => extras.getString(Intent.EXTRA_TEXT) :: Nil
            case Intent.ACTION_SEND_MULTIPLE => extras.getStringArray(Intent.EXTRA_TEXT).toList
            case _ => Nil
         }
         case _ => Nil
      }
      var attachments: List[EmailHelper.Attachment] = mimeType match {
         case "text/plain" => Nil
         case _ => intent.getAction match {
            case Intent.ACTION_SEND => createAttachment(mimeType, extras.getParcelable(Intent.EXTRA_STREAM).asInstanceOf[Uri]) :: Nil
            case Intent.ACTION_SEND_MULTIPLE =>
               val uriList: List[Parcelable] = extras.getParcelableArrayList(Intent.EXTRA_STREAM).toList
               uriList.map(p => createAttachment(mimeType, p.asInstanceOf[Uri]))
            case _ => Nil
         }
      }
      getScreenshotAttachment(extras).foreach(s => attachments = s :: attachments)

      import scala.concurrent.ExecutionContext.Implicits.global
      Future(EmailHelper.sendEmailWithShare(emailConfig, subjectOpt, texts, attachments)).map {
         case EmailHelper.EmailFailed(server, error) =>
            DialogUtils.toastLong(this, s"Failed to send email to ${emailConfig.targetEmail}!\n$server: $error!")
            MediaPlayer.create(getApplicationContext(), R.raw.failure).start()
            finish()
         case _ =>
            DialogUtils.toast(this, s"Email has been sent to ${emailConfig.targetEmail}!")
            MediaPlayer.create(getApplicationContext(), R.raw.success).start()
            finish()
      }
   }

   var t: AnyRef = null

   private def createAttachment(mimeType: String, uri: Uri): Attachment = {
      val source = getContentResolver().openInputStream(uri)
      val data = new ByteArrayOutputStream()
      BasicIO.transferFully(source, data)
      Attachment(mimeType, uri.getLastPathSegment, data.toByteArray)
   }

   private def getScreenshotAttachment(extras: Bundle): Option[Attachment] = {
      if(extras.containsKey("share_screenshot")) {
         try {
            val screenshotObj = extras.get("share_screenshot")
            if(screenshotObj.isInstanceOf[Bitmap]) {
               val screenshotBitmap = screenshotObj.asInstanceOf[Bitmap]
               val sourceSize = ZSize(screenshotBitmap.getWidth, screenshotBitmap.getHeight)
               def findDivider(width: Int, divider: Int): Int = if(width / divider <= 1024) divider else findDivider(width, divider * 2)
               val multiplier = 1f / findDivider(sourceSize.width, 1)
               val targetSize = sourceSize.scale(multiplier, multiplier)
               val matrix = createMatrix(sourceSize, 0, targetSize)
               val targetBitmap = Bitmap.createBitmap(targetSize.width, targetSize.height, Bitmap.Config.ARGB_8888)
               val canvas = new Canvas(targetBitmap)
               canvas.setMatrix(matrix)
               canvas.drawBitmap(screenshotBitmap, 0, 0, null)
               val buffer = new ByteArrayOutputStream()
               targetBitmap.compress(Bitmap.CompressFormat.JPEG, 91, buffer)
               return Some(Attachment("image/jpeg", "screenshot.jpg", buffer.toByteArray))
            }
         }
         catch {
            case ex: Throwable => logError("Cannot process screenshot", ex)
         }
      }
      None
   }

   private def showSettings() {
      //TODO code duplicate from MainAction
      val emailSettingsRequest = ActivityRequest(classOf[SettingsActivity], classOf[Option[EmailConfig]]) {
         case None => ; //ignore
         case Some(emailConfig) =>
            val preferencesTx = preferences.edit()
            preferencesTx.putString(MainActivity.emailConfigStateKey, emailConfig.asString)
            preferencesTx.commit()
      }
      startActivityForResponse(emailSettingsRequest, EmailConfig(preferences.getString(MainActivity.emailConfigStateKey, "")))
   }
}
