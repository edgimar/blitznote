package com.volidar.blitznote.client.android

import com.volidar.blitznote.client.android.common.{SendEmailExceptionHandler, DialogUtils, BaseActivity}
import android.os.{Debug, Bundle}
import android.widget.ImageView
import android.view.{ContextMenu, KeyEvent, MenuItem, Menu, Window, MotionEvent, View}
import android.view.View.{OnTouchListener, OnKeyListener}
import android.view.ViewTreeObserver.OnPreDrawListener
import scala.Some
import android.content.Context
import android.util.DisplayMetrics
import android.widget.ImageView.ScaleType
import com.volidar.blitznote.client.android.data.{Drawer, EmailConfig}
import com.volidar.blitznote.client.android.settings.SettingsActivity
import java.text.SimpleDateFormat
import java.util.Date
import rx.subjects.PublishSubject
import com.volidar.blitznote.client.android.UserLowActionHandler.ActionCancel
import com.volidar.blitznote.client.android.data.base.ZSize
import android.media.MediaPlayer
import android.graphics.{Canvas, Bitmap}
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import android.view.ContextMenu.ContextMenuInfo
import scala.util.Try

object MainActivity {
   @volatile var instanceOpt: Option[MainActivity] = None

   val bitmapStateKey = "bitmap"
   val emailConfigStateKey = "emailConfig"

   def getMyPreferences(context: Context) = context.getSharedPreferences(classOf[MainActivity].getName, Context.MODE_MULTI_PROCESS)
}

class MainActivity extends BaseActivity(R.layout.activity_main) {
   import com.volidar.blitznote.client.android.MainActivity._

   MainActivity.instanceOpt = Some(this)
   private lazy val preferences = getMyPreferences(this)

   private var drawerOpt: Option[Drawer] = None
   private var imgMain: ImageView = _

   private val userLowActions = PublishSubject.create[UserLowActionHandler.UserLowAction]()
   private val userActions = PublishSubject.create[UserActionHandler.UserAction]()

   override protected def onPause() {
      userLowActions.onNext(ActionCancel)
      for(drawer <- drawerOpt) {
         future {
            BlitzNoteAppWidgetProvider.updateWidgets(this, drawer.lastHistory)
         }
         val preferencesEditor = preferences.edit()
         preferencesEditor.putString(bitmapStateKey, drawer.prepareForStoring)
         preferencesEditor.commit()
      }

      super.onPause()
   }

   override protected def onRestart() {
      super.onRestart()
      for(drawer <- drawerOpt) {
         drawer.reinitialize()
      }
   }

   protected override def onCreate(savedInstanceState: Bundle) {
      SendEmailExceptionHandler.applyToCurrentThread()
      super.onCreate(savedInstanceState)

      imgMain = findView(TR.imgMain)
      registerForContextMenu(imgMain)

      imgMain.getViewTreeObserver.addOnPreDrawListener(new OnPreDrawListener {
         override def onPreDraw(): Boolean = drawerOpt match {
            case Some(_) => true
            case None =>
               val metrics = new DisplayMetrics
               getWindowManager.getDefaultDisplay.getMetrics(metrics)
               val orientation = getWindowManager.getDefaultDisplay.getRotation
               val noteData = preferences.getString(bitmapStateKey, "")

               val drawer = new Drawer(metrics, orientation, getResources, ZSize(imgMain.getWidth, imgMain.getHeight), noteData)
               drawerOpt = Some(drawer)
               userLowActions.subscribe(new UserLowActionHandler(userActions, drawer.dpmm))
               userActions.subscribe(new UserActionHandler(MainActivity.this, imgMain, drawer))

               imgMain.setScaleType(ScaleType.MATRIX)
               imgMain.setImageMatrix(drawer.matrixNoteToView)
               imgMain.setImageDrawable(drawer.drawable)
               imgMain.setFocusableInTouchMode(true)
               imgMain.setOnTouchListener(new OnTouchListener {
                  override def onTouch(v: View, event: MotionEvent): Boolean = {
                     import com.volidar.blitznote.client.android.UserLowActionHandler._
                     event.getAction & MotionEvent.ACTION_MASK match {
                        case MotionEvent.ACTION_DOWN => userLowActions.onNext(ActionDown(event))
                        case MotionEvent.ACTION_UP => userLowActions.onNext(ActionUp(event))
                        case MotionEvent.ACTION_POINTER_DOWN => userLowActions.onNext(ActionDown(event))
                        case MotionEvent.ACTION_POINTER_UP => userLowActions.onNext(ActionUp(event))
                        case MotionEvent.ACTION_MOVE => userLowActions.onNext(ActionMove(event))
                        case MotionEvent.ACTION_CANCEL => userLowActions.onNext(ActionCancel)
                        case _ => return false;
                     }
                     true
                  }
               })
               imgMain.setOnKeyListener(new OnKeyListener {
                  override def onKey(v: View, keyCode: Int, event: KeyEvent): Boolean = {
                     import com.volidar.blitznote.client.android.UserLowActionHandler._
                     (keyCode, event.getAction) match {
                        case (KeyEvent.KEYCODE_BACK, KeyEvent.ACTION_DOWN) => userLowActions.onNext(KeyDownBack)
                        case (KeyEvent.KEYCODE_BACK, KeyEvent.ACTION_UP) => userLowActions.onNext(KeyUpBack)
                        //case (KeyEvent.KEYCODE_NUMPAD_4, KeyEvent.ACTION_DOWN) => userLowActions.onNext(KeyDownStylus)
                        //case (KeyEvent.KEYCODE_NUMPAD_4, KeyEvent.ACTION_UP) => userLowActions.onNext(KeyDownStylus)
                        case _ => return false
                     }
                     true
                  }
               })

               TipManager.showTip(MainActivity.this)
               true
         }
      })
   }

   def sendEmail() {
      EmailConfig(preferences.getString(emailConfigStateKey, "")) match {
         case Some(emailConfig) => sendEmail(emailConfig)
         case None =>
            val emailSettings = ActivityRequest(classOf[SettingsActivity], classOf[Option[EmailConfig]]) {
               case None => DialogUtils.toast(this, "Configuration has been cancelled!")
               case Some(emailConfig) =>
                  val preferencesTx = preferences.edit()
                  preferencesTx.putString(emailConfigStateKey, emailConfig.asString)
                  preferencesTx.commit()
                  sendEmail(emailConfig)
            }
            startActivityForResponse(emailSettings, None)
      }
   }

   private def sendEmail(emailConfig: EmailConfig) {
      for(drawer <- drawerOpt) {
         DialogUtils.toast(this, s"Sending email to ${emailConfig.targetEmail}")
         val noteData = drawer.prepareForEmail

         import scala.concurrent.ExecutionContext.Implicits.global
         Future(EmailHelper.sendEmailWithNote(emailConfig, noteData)).map {
            case EmailHelper.EmailFailed(server, error) =>
               DialogUtils.toastLong(this, s"Failed to send email to ${emailConfig.targetEmail}!\n$server: $error!")
               MediaPlayer.create(getApplicationContext(), R.raw.failure).start()
            case _ =>
               DialogUtils.toast(this, s"Email has been sent to ${emailConfig.targetEmail}!")
               MediaPlayer.create(getApplicationContext(), R.raw.success).start()
         }
      }
   }

   override def onCreateOptionsMenu(menu: Menu): Boolean = {
      super.onCreateOptionsMenu(menu)
      getMenuInflater.inflate(R.menu.main, menu)
      true
   }

   override def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo) = {
      super.onCreateContextMenu(menu, v, menuInfo)
      getMenuInflater.inflate(R.menu.main, menu)
   }

   def openContextMenu(): Unit = openContextMenu(imgMain)
   override def onContextItemSelected(item: MenuItem): Boolean = if(onItemSelected(item)) true else super.onContextItemSelected(item)
   override def onOptionsItemSelected(item: MenuItem): Boolean = if(onItemSelected(item)) true else super.onOptionsItemSelected(item)
   private def onItemSelected(item: MenuItem): Boolean = {
      item.getItemId match {
         case R.id.mnuSendEmail => ;
            userActions.onNext(UserActionHandler.SendEmail)
            true
         case R.id.mnuSettings =>
            showSettings()
            true
         case R.id.mnuEraseAll => ;
            userActions.onNext(UserActionHandler.EraseAll)
            true
         case R.id.mnuUndoAll => ;
            userActions.onNext(UserActionHandler.UndoAll)
            true
         case R.id.mnuChangePencil => ;
            userActions.onNext(UserActionHandler.ChangePencil)
            true
         case R.id.mnuChangePencilToEraser => ;
            userActions.onNext(UserActionHandler.ChangePencilToEraser)
            true
         case R.id.mnuEraseSelection => ;
            userActions.onNext(UserActionHandler.EraseSelection)
            true
         case R.id.mnuRedo => ;
            userActions.onNext(UserActionHandler.Redo)
            true
         //case R.id.mnuMemoryDump =>
         //   Try(Debug.dumpHprofData("/sdcard/download/dump_" + new SimpleDateFormat("MMdd-HHmmss").format(new Date) + ".andr.hprof"))
         //   true
         case R.id.mnuExit => ;
            finish()
            true
         case _ => false
      }
   }

   override def onCreateThumbnail(outBitmap: Bitmap, canvas: Canvas) = {
      drawerOpt match {
         case None => super.onCreateThumbnail(outBitmap, canvas)
         case Some(drawer) =>
            drawer.drawThumbnail(outBitmap, canvas)
            true
      }
   }

   def showSettings() {
      val emailSettingsRequest = ActivityRequest(classOf[SettingsActivity], classOf[Option[EmailConfig]]) {
         case None => ; //ignore
         case Some(emailConfig) =>
            val preferencesTx = preferences.edit()
            preferencesTx.putString(emailConfigStateKey, emailConfig.asString)
            preferencesTx.commit()
      }
      startActivityForResponse(emailSettingsRequest, EmailConfig(preferences.getString(emailConfigStateKey, "")))
   }

   def sendToDeveloper(message: String) {
      for(drawer <- drawerOpt) {
         import ExecutionContext.Implicits.global
         drawer.sendToDeveloper(message).onComplete(_.get match {
            case _: EmailHelper.EmailSent => DialogUtils.toast(MainActivity.instanceOpt.get, "Message to Developer has been sent")
            case EmailHelper.EmailFailed(_, error) => DialogUtils.toastLong(MainActivity.instanceOpt.get, s"Message to Developer cannot be sent:\n$error")
         })
      }
   }

   def clearHistory() {
      for(drawer <- drawerOpt) {
         drawer.clearHistory()
         userActions.onNext(UserActionHandler.Rollback)
      }
   }

   def recycle() {
      for(drawer <- drawerOpt) {
         drawer.recycle()
      }
   }
}
