package com.volidar.blitznote.client.android.common

import android.util.Log

trait Logging {
   protected def logError(message: => Any, exception: Throwable = null) = log(Log.ERROR, message, exception)
   protected def logWarn(message: => Any, exception: Throwable = null) = log(Log.WARN, message, exception)
   protected def logInfo(message: => Any, exception: Throwable = null) = log(Log.INFO, message, exception)
   protected def logDebug(message: => Any, exception: Throwable = null) = log(Log.DEBUG, message, exception)
   protected def logTrace(message: => Any, exception: Throwable = null) = log(Log.VERBOSE, message, exception)

   protected def log(level: Int, message: => Any, exception: Throwable) {
      val logCategory = "ZLog"
      val className = getClass.getName

      if(Log.isLoggable(logCategory, level)) {
         val messageStr = className + ": " + String.valueOf(message)
         (level, exception == null) match {
            case (Log.VERBOSE, true) => Log.v(logCategory, messageStr)
            case (Log.VERBOSE, false) => Log.v(logCategory, messageStr, exception)

            case (Log.DEBUG, true) => Log.d(logCategory, messageStr)
            case (Log.DEBUG, false) => Log.d(logCategory, messageStr, exception)

            case (Log.INFO, true) => Log.i(logCategory, messageStr)
            case (Log.INFO, false) => Log.i(logCategory, messageStr, exception)

            case (Log.WARN, true) => Log.w(logCategory, messageStr)
            case (Log.WARN, false) => Log.w(logCategory, messageStr, exception)

            case (_ /*Log.ERROR*/ , true) => Log.e(logCategory, messageStr)
            case (_ /*Log.ERROR*/ , false) => Log.e(logCategory, messageStr, exception)
         }
      }
      Unit
   }
}
