package com.volidar.blitznote.client.android.settings

import com.volidar.blitznote.client.android.common.{DialogUtils, Response, BaseActivity}
import com.volidar.blitznote.client.android.{MainActivity, TR, R}
import android.view.View
import com.volidar.blitznote.client.android.data.EmailConfig
import android.os.Bundle
import scala.concurrent.Future

class SettingsActivity extends BaseActivity[Option[EmailConfig]](R.layout.activity_settings) with Response[Option[EmailConfig]] {
   private lazy val edtTargetEmail = findView(TR.SettingsEmail_edtTargetEmail)
   private lazy val edtUserName = findView(TR.SettingsEmail_edtUserName)
   private lazy val edtPassword = findView(TR.SettingsEmail_edtPassword)
   private lazy val edtSendFrom = findView(TR.SettingsEmail_edtSendFrom)
   private lazy val edtSmtpServer = findView(TR.SettingsEmail_edtSmtpServer)
   private lazy val edtServerPort = findView(TR.SettingsEmail_edtServerPort)
   private lazy val chkAdditionalParameters = findView(TR.SettingsEmail_chkAdditionalParameters)
   private lazy val chkUseSsl = findView(TR.SettingsEmail_chkUseSsl)
   private lazy val grpAdditionalParameters = findView(TR.SettingsEmail_grpAdditionalParameters)
   private lazy val edtMessageToDeveloper = findView(TR.SettingsEmail_edtMessageToDeveloper)

   protected override def onCreate(savedInstanceState: Bundle) = {
      super.onCreate(savedInstanceState)

      request match {
         case None => ;//nothing to do
         case Some(emailConfig) =>
            edtTargetEmail.setText(emailConfig.targetEmail)
            if(emailConfig.smtpServer.isEmpty) {
               chkAdditionalParameters.setChecked(false)
               chkAdditionalParametersOnClick(chkAdditionalParameters)
            } else {
               chkAdditionalParameters.setChecked(true)
               chkAdditionalParametersOnClick(chkAdditionalParameters)
               edtUserName.setText(emailConfig.userName)
               edtPassword.setText(emailConfig.password)
               edtSmtpServer.setText(emailConfig.smtpServer)
               chkUseSsl.setChecked(emailConfig.useSsl)
               edtServerPort.setText(emailConfig.port.toString)
               edtSendFrom.setText(emailConfig.from)
            }
      }
   }

   def btnOkOnClick(view: View) {
      val targetEmail = edtTargetEmail.getText.toString
      if(targetEmail.isEmpty) {
         returnResult(None)
         return
      }

      if(!chkAdditionalParameters.isChecked) {
         returnResult(Some(new EmailConfig(targetEmail)))
         return
      }

      val portStr = edtServerPort.getText.toString
      val port = try {portStr.toInt}
      catch {
         case ex: Throwable =>
            edtServerPort.setError("Cannot parse SMTP port. In most cases it's 25, for SSL it's 465.")
            return
      }
      if(port <= 0 || port > 65535) {
         edtServerPort.setError("SMTP port is out of range (1 .. 65535). In most cases it's 25, for SSL it's 465.")
         return
      }

      returnResult(Some(new EmailConfig(targetEmail,
         edtUserName.getText.toString, edtPassword.getText.toString, edtSendFrom.getText.toString,
         edtSmtpServer.getText.toString, port, chkUseSsl.isChecked)))
   }

   def btnClearHistoryOnClick(view: View) = MainActivity.instanceOpt.map(_.clearHistory())
   def btnSendToDeveloperOnClick(view: View) {
      MainActivity.instanceOpt.map(_.sendToDeveloper(edtMessageToDeveloper.getText.toString))
   }

   def chkAdditionalParametersOnClick(view: View) {
      if(chkAdditionalParameters.isChecked) {
         grpAdditionalParameters.setVisibility(View.VISIBLE)
         if(edtUserName.getText.toString.isEmpty) edtUserName.setText(edtTargetEmail.getText.toString)
         if(edtSendFrom.getText.toString.isEmpty) edtSendFrom.setText(edtTargetEmail.getText.toString)
         if(edtSmtpServer.getText.toString.isEmpty) {
            val atDomain = edtTargetEmail.getText.toString.dropWhile(_ != '@')
            if(atDomain.nonEmpty) edtSmtpServer.setText(atDomain.tail)
         }
      } else {
         grpAdditionalParameters.setVisibility(View.GONE)
      }
   }
   def chkUseSslOnClick(view: View) = edtServerPort.setText(if(chkUseSsl.isChecked) "465" else "25")
}
