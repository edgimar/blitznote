package com.volidar.blitznote.client.android

import javax.mail.internet.{InternetAddress, MimeMultipart, MimeBodyPart, MimeMessage}
import org.xbill.DNS.Lookup
import org.xbill.DNS.Type
import org.xbill.DNS.MXRecord
import java.util.{Properties, Date}
import java.text.SimpleDateFormat
import javax.mail.{AuthenticationFailedException, PasswordAuthentication, Transport, Message, Session}
import javax.activation.DataHandler
import com.volidar.blitznote.client.android.common.{SendEmailExceptionHandler, ExceptionUtils, Logging}
import javax.mail.util.ByteArrayDataSource
import com.volidar.blitznote.client.android.data.EmailConfig
import java.io.{ObjectOutputStream, ByteArrayOutputStream}
import com.volidar.blitznote.client.android.Hardcode._
import android.text.TextUtils

object EmailHelper extends Logging {
   sealed trait EmailResult
   case class EmailSent(server: String) extends EmailResult
   case class EmailFailed(server: String, error: String) extends EmailResult {
      def this(server: String, ex: Throwable) = this(server, EmailFailed.getEmailFailureMessage(ex))
   }
   object EmailFailed {
      def getEmailFailureMessage(ex: Throwable): String = ex match {
         case _: AuthenticationFailedException => "Please check user name and password"
         case _ => ex.getMessage
      }
   }

   def sendEmailWithNote(emailConfig: EmailConfig, note: Array[Byte]): EmailResult = {
      SendEmailExceptionHandler.applyToCurrentThread()
      findServersAndSendEmail(emailConfig, prepareNoteMessage(note))
   }

   private val imgNoteCid = "note.png"
   private val imgPaypalCid = "paypal.gif"
   private val htmlBodyStr = s"""<p style='margin:0cm;margin-bottom:.0001pt;font-size:11pt;font-family:"Calibri","sans-serif";'>
                                |Hello,<br />
                                |<br />
                                |please process:<br />
                                |<img src="cid:$imgNoteCid" /><br />
                                |<br />
                                |Best regards, Blitz Note.<br />
                                |Help us to make it even better! <a href="http://www.volidar.com/donate"><img src="cid:$imgPaypalCid" /></a><br />
                                |</p>""".stripMargin
   private val plainBodyStr = """Hello,
                                |
                                |please process the attached note.
                                |
                                |Best regards, Blitz Note.
                                | """.stripMargin

   private def prepareNoteMessage(imgNote: Array[Byte])(message: MimeMessage) {
      val now = new Date
      val textTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(now)
      val fileTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(now)

      message.setSubject(s"BlitzNote ($textTime)")
      message.setText(plainBodyStr, "UTF-8", "plain")

      val textPart = new MimeBodyPart()
      textPart.setText(htmlBodyStr, "UTF-8", "html")

      val imgNotePart = new MimeBodyPart()
      imgNotePart.setContentID(imgNoteCid)
      imgNotePart.setDataHandler(new DataHandler(new ByteArrayDataSource(imgNote, "image/png")))
      imgNotePart.setFileName(s"BlitzNote_$fileTime.png")

      val imgPaypalPart = new MimeBodyPart()
      imgPaypalPart.setContentID(imgPaypalCid)
      imgPaypalPart.setDataHandler(new DataHandler(new ByteArrayDataSource(imgPaypal, "image/gif")))
      imgPaypalPart.setFileName(s"paypal.gif")

      val multipart = new MimeMultipart()
      multipart.addBodyPart(textPart)
      multipart.addBodyPart(imgNotePart)
      multipart.addBodyPart(imgPaypalPart)
      message.setContent(multipart)
   }

   case class Attachment(mimeType: String, fileName: String, data: Array[Byte])
   def sendEmailWithShare(emailConfig: EmailConfig, subjectOpt: Option[String], texts: List[String], attachments: List[Attachment]): EmailResult = {
      SendEmailExceptionHandler.applyToCurrentThread()
      findServersAndSendEmail(emailConfig, (message) => {
         val now = new Date
         subjectOpt match {
            case Some(subject) =>
               val textTime = new SimpleDateFormat("HH:mm").format(now)
               message.setSubject(s"BlitzShare ($textTime): $subject")
            case None =>
               val textTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(now)
               message.setSubject(s"BlitzShare ($textTime)")
         }

         val plainHeader = s"""Hello,
                              |
                              |you have shared following information:
                              |""".stripMargin
         val plainFooter = s"""
                              |Best regards, Blitz Note.
                              |""".stripMargin
         val htmlHeader = s"""<p style='margin:0cm;margin-bottom:.0001pt;font-size:11pt;font-family:"Calibri","sans-serif";'>
                             |Hello,<br />
                             |<br />
                             |you have shared following information:<br />
                             |""".stripMargin
         val htmlFooter = s"""
                             |</p>
                             |<p style='margin:0cm;margin-bottom:.0001pt;font-size:11pt;font-family:"Calibri","sans-serif";'>
                             |<br />
                             |Best regards, Blitz Note.<br />
                             |Help us to make it even better! <a href="http://www.volidar.com/donate"><img src="cid:$imgPaypalCid" /></a><br />
                             |</p>""".stripMargin
         message.setText(plainHeader + texts.map(_ + "\r\n").mkString("================================\r\n") + plainFooter, "UTF-8", "plain")

         val attachmentsWithIndex = attachments.zipWithIndex
         def attachmentCid(index: Int) = s"att_$index"

         val imageAttachmentsWithIndex = attachmentsWithIndex.filter(_._1.mimeType.startsWith("image/"))

         def toHtmlList(items: Seq[String]) = "<ul style='margin:0cm;margin-bottom:.0001pt;font-size:11pt;font-family:\"Calibri\",\"sans-serif\";'>\r\n" +
            items.map(item => s"   <li>$item</li>\r\n").mkString + "</ul>"

         val htmlMain = toHtmlList(texts.map(TextUtils.htmlEncode(_))) +
            (if(texts.nonEmpty && imageAttachmentsWithIndex.nonEmpty) "<br />\r\n" else "") +
            toHtmlList(imageAttachmentsWithIndex.map(_._2).map(index => s"<img src='cid:${attachmentCid(index)}' style='vertical-align: middle;' />"))

         val htmlPart = new MimeBodyPart()
         htmlPart.setText(htmlHeader + htmlMain + htmlFooter, "UTF-8", "html")

         val imgPaypalPart = new MimeBodyPart()
         imgPaypalPart.setContentID(imgPaypalCid)
         imgPaypalPart.setDataHandler(new DataHandler(new ByteArrayDataSource(imgPaypal, "image/gif")))
         imgPaypalPart.setFileName("paypal.gif")

         val multipart = new MimeMultipart()
         multipart.addBodyPart(htmlPart)
         multipart.addBodyPart(imgPaypalPart)
         message.setContent(multipart)

         for((attachment, index) <- attachmentsWithIndex) {
            val part = new MimeBodyPart()
            part.setContentID(attachmentCid(index))
            part.setDataHandler(new DataHandler(new ByteArrayDataSource(attachment.data, attachment.mimeType)))
            part.setFileName(attachment.fileName)
            multipart.addBodyPart(part)
         }
      })
   }

   val developerEmailConfig = new EmailConfig("support@volidar.com", "", "", "support@volidar.com", "mail.volidar.com", 25, useSsl = false)
   def sendEmailToDeveloper(subject: String, body: String, obj: AnyRef) = {
      findServersAndSendEmail(developerEmailConfig, (message) => {
         message.setSubject(subject)

         if(obj == null) {
            message.setText(body, "UTF-8", "plain")
         } else {
            try {
               val objData = new ByteArrayOutputStream()
               val out = new ObjectOutputStream(objData)
               out.writeObject(obj)
               out.close()

               val textPart = new MimeBodyPart()
               textPart.setText(body, "UTF-8", "plain")

               val objPart = new MimeBodyPart()
               objPart.setDataHandler(new DataHandler(new ByteArrayDataSource(objData.toByteArray, "application/octet-stream")))
               objPart.setFileName("object.dat")

               val multipart = new MimeMultipart()
               multipart.addBodyPart(textPart)
               multipart.addBodyPart(objPart)
               message.setContent(multipart)
            }
            catch {
               case ex: Exception => message.setText(body + "\r\n\r\nSerialization Failed:\r\n" + ExceptionUtils.getMessageAndStackTrace(ex), "UTF-8", "plain")
            }
         }
      })
   }

   private def findServersAndSendEmail(emailConfig: EmailConfig, messageComposer: (MimeMessage) => Unit): EmailResult = {
      def getMailServers(emailDomain: String): Seq[String] = {
         val entries = new Lookup(emailDomain, Type.MX).run()
         if(entries == null || entries.isEmpty) return List(emailDomain)
         entries.sortBy(_.asInstanceOf[MXRecord].getPriority).map(_.asInstanceOf[MXRecord].getTarget.toString(true)).toList
      }
      def trySend(config: EmailConfig): EmailResult = {
         try {
            sendEmailToServer(config, messageComposer)
            EmailSent(config.smtpServer)
         }
         catch {
            case ex: Throwable =>
               logWarn(s"Failed: ${config.smtpServer}", ex);
               new EmailFailed(config.smtpServer, ex)
         }
      }

      if(emailConfig.smtpServer.nonEmpty) {
         trySend(emailConfig)
      } else {
         val mailServers = getMailServers(emailConfig.targetEmail.dropWhile(_ != '@').tail)
         val resultOpt = mailServers.foldLeft(None: Option[EmailResult]) {
            case (result@Some(_: EmailSent), _) => result
            case (_, smtpServer) =>
               val trivialConfig = EmailConfig(emailConfig.targetEmail, "", "", emailConfig.targetEmail, smtpServer, 25, useSsl = false)
               Some(trySend(trivialConfig))
         }
         resultOpt match {
            case Some(result) => result
            case None => EmailFailed("", "No SMTP Servers have been found")
         }
      }
   }

   private def sendEmailToServer(emailConfig: EmailConfig, messageComposer: (MimeMessage) => Unit) {
      import java.lang.Boolean.{valueOf => JBool}

      val properties = new Properties()
      properties.put("mail.smtp.starttls.enable", JBool(emailConfig.useSsl))
      properties.put("mail.smtp.ssl.enable", JBool(emailConfig.useSsl))
      properties.put("mail.smtp.ssl.trust", emailConfig.smtpServer)
      properties.put("mail.smtp.host", emailConfig.smtpServer)
      properties.put("mail.smtp.port", new Integer(emailConfig.port))
      properties.put("mail.smtp.auth", JBool(emailConfig.password.nonEmpty))

      val session = if(emailConfig.password.isEmpty) {Session.getInstance(properties)}
      else {
         Session.getInstance(properties, new javax.mail.Authenticator() {
            protected override def getPasswordAuthentication = new PasswordAuthentication(emailConfig.userName, emailConfig.password)
         })
      }
      //session.setDebug(true)//TODO check if this debug code is needed

      val message = new MimeMessage(session)
      message.setFrom(new InternetAddress(emailConfig.from))
      message.setRecipients(Message.RecipientType.TO, emailConfig.targetEmail)
      messageComposer(message)

      if(emailConfig.password.isEmpty) {
         Transport.send(message)
      }
      else {
         val transport = session.getTransport(if(emailConfig.useSsl) "smtps" else "smtp")
         transport.connect(emailConfig.smtpServer, emailConfig.port, emailConfig.userName, emailConfig.password)
         transport.sendMessage(message, message.getAllRecipients)
         transport.close()
      }
   }
}

//How to read Accounts: <uses-permission android:name="android.permission.GET_ACCOUNTS"/>
//String emailID="";
//Account[] accounts=AccountManager.get(mContext).getAccounts();
//for(Account account:accounts) {
//   if(Patterns.EMAIL_ADDRESS.matcher(account.name).matches()) {
//      emailID=emailID+account.name+"\n\t\t\t\t";
//   }
//}
