package com.volidar.blitznote.client.android.data

import android.graphics.{Color, DashPathEffect, Paint, PorterDuff, PorterDuffXfermode}
import com.volidar.blitznote.client.android.data.base._

object DrawObjects {
   val gridColor = 0xFFC0C0B0
   //bebcad
   val subgridColor = 0xFFD8D8D4
   val backgroundColor = 0xFFF0F0E8
   //f1f1e7
   val cursorColor = 0xFF000000
}

class DrawObjects(val params: DrawParams, val targetNoteSize: ZSize, val targetViewSize: ZSize, val orientation: Int) {
   val eraserSize = (params.eraserWidthFromOne * targetNoteSize.width + 1).toInt
   val pencilSize = (params.pencilWidthFromOne * targetNoteSize.width + 1).toInt
   val eraserShift = ZPoint(0f, -2.5f * params.dpmm)
   //eraser centrum is moved up 2.5mm
   val paintEraser = createPaint(eraserSize, _.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR)))
   //val paintEraser = createPaint(eraserSize, _.setColor(Color.YELLOW))
   val paintPencils = (paintEraser :: params.pencilColors.map(color => createPaint(pencilSize, p => p.setColor(color)))).toArray
   def paint(index: Int) = paintPencils(index + 1) //-1 => eraser : idx=0, 0 => pencil0 : idx=1
   val paintSelectionBorderEraser = createPaint(pencilSize + 3f, {
      p =>
         p.setAntiAlias(false)
         p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR))
   })
   val paintSelectionEraser = createPaint(pencilSize + 3f, {
      p =>
         p.setAntiAlias(false)
         p.setStyle(Paint.Style.FILL_AND_STROKE)
         p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR))
   })
   val paintSelection = createPaint(2, { p => p.setColor(DrawObjects.gridColor); p.setPathEffect(new DashPathEffect(Array(10f, 20f), 0))})

   val paintCursor = createPaint(1, _.setColor(DrawObjects.cursorColor))
   val paintSubgrid = createPaint(1, _.setColor(DrawObjects.subgridColor))
   val paintGrid = createPaint(1, _.setColor(DrawObjects.gridColor))

   val matrixNoteToView = createMatrix(targetNoteSize, -orientation, targetViewSize)
   val matrixViewToNote = invertMatrix(matrixNoteToView)
   val matrixNoteToOne = createMatrix(targetNoteSize, 0, ZSize.one)
   val matrixViewToOne = createMatrix(targetViewSize, orientation, ZSize.one)
   val matrixOneToNote = createMatrix(ZSize.one, 0, targetNoteSize)
   val matrixOneToView = createMatrix(ZSize.one, -orientation, targetViewSize)

   private def createPaint(size: Float, postActions: (Paint) => Unit): Paint = {
      val result = new Paint
      result.setAntiAlias(true)
      result.setStrokeWidth(size)
      result.setStrokeCap(Paint.Cap.ROUND)
      result.setStrokeJoin(Paint.Join.ROUND)
      result.setStyle(Paint.Style.STROKE)
      postActions(result)
      result
   }
}
