package com.volidar.blitznote.client.android

import android.appwidget.{AppWidgetManager, AppWidgetProvider}
import com.volidar.blitznote.client.android.common.{SendEmailExceptionHandler, Logging}
import android.content.{ComponentName, Intent, Context}
import android.os.Bundle
import android.widget.RemoteViews
import android.graphics.{Canvas, Color, Bitmap}
import com.volidar.blitznote.client.android.data.base._
import com.volidar.blitznote.client.android.data.{DrawObjects, History, Drawer}
import com.volidar.blitznote.client.android.MainActivity.bitmapStateKey
import android.app.PendingIntent
import com.volidar.blitznote.client.android.v10.AppWidgetManagerV10

class BlitzNoteAppWidgetProvider extends AppWidgetProvider with Logging {
   //called once on addToScreen, not called for second widget
   //override def onEnabled(context: Context) {
   //   super.onEnabled(context)
   //}
   ////called on complete delete from screen
   //override def onDisabled(context: Context) = super.onDisabled(context)
   //
   ////called on remove from WidgetHost
   //override def onDeleted(context: Context, appWidgetIds: Array[Int]) = super.onDeleted(context, appWidgetIds)
   //
   override def onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: Array[Int]) {
      SendEmailExceptionHandler.applyToCurrentThread()
      super.onUpdate(context, appWidgetManager, appWidgetIds)
      BlitzNoteAppWidgetProvider.updateWidgets(context, None, appWidgetManager, appWidgetIds)
   }
   override def onAppWidgetOptionsChanged(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int, newOptions: Bundle) {
      SendEmailExceptionHandler.applyToCurrentThread()
      super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
      BlitzNoteAppWidgetProvider.updateWidgets(context, None, appWidgetManager, Map(appWidgetId -> newOptions))
   }
   //override def onReceive(context: Context, intent: Intent) = super.onReceive(context, intent)
}

object BlitzNoteAppWidgetProvider extends Logging {
   def updateWidgets(context: Context, lastHistory: History) {
      val appWidgetManager = AppWidgetManager.getInstance(context.getApplicationContext())
      val appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, classOf[BlitzNoteAppWidgetProvider]))
      updateWidgets(context, Some(lastHistory), appWidgetManager, appWidgetIds)
   }

   private def updateWidgets(context: Context, historyOpt: Option[History], appWidgetManager: AppWidgetManager, appWidgetIds: Array[Int]) {
      if(appWidgetIds.nonEmpty) {
         val appWidgetIdsWithOptions = appWidgetIds.map(id => id -> AppWidgetManagerV10.getAppWidgetOptions(appWidgetManager, id)).toMap
         updateWidgets(context, historyOpt, appWidgetManager, appWidgetIdsWithOptions)
      }
   }

   private def updateWidgets(context: Context, historyOpt: Option[History], appWidgetManager: AppWidgetManager, appWidgetIdsWithOptions: Map[Int, Bundle]) {
      def save(real: Int, fallback: Int): Int = if(real != 0) real else fallback

      val metrics = context.getResources.getDisplayMetrics
      val maxWidthDp = save(appWidgetIdsWithOptions.map(_._2.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)).max, 256)
      val maxHeightDp = save(appWidgetIdsWithOptions.map(_._2.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)).max, 256)
      val maxViewSize = ZSize(maxWidthDp, maxHeightDp).scale(metrics.density, metrics.density)//scale from DP to PX

      val history = historyOpt match {
         case Some(knownHistory) => knownHistory
         case _ =>
            val storedHistory = MainActivity.getMyPreferences(context).getString(bitmapStateKey, "")
            History.restoreHistory(storedHistory, metrics, 0, maxViewSize)
      }

      for((appWidgetId, options) <- appWidgetIdsWithOptions.toList.reverse) {
         val widthDp = save(options.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH), 256)
         val heightDp = save(options.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT), 256)
         val viewSize = ZSize(maxWidthDp, maxHeightDp).scale(metrics.density, metrics.density)//scale from DP to PX

         val (note, withBackground) = createBySize(viewSize.enforceWidthToHeight(history.params.noteWidthToHeight), s => (createBitmap(s), createBitmap(s)))
         val bitmapSize = ZSize(note.getWidth, note.getHeight)
         //Both params are bitmapSize because we ignore real size and going to scale unproportionally
         val drawObjects = new DrawObjects(history.params, bitmapSize, bitmapSize, 0)
         Drawer.drawLatestHistoryOnBitmap(note, Color.TRANSPARENT, history, drawObjects)

         Drawer.drawBackground(withBackground, bitmapSize, drawObjects)
         val bitmapCanvas = new Canvas(withBackground)
         bitmapCanvas.drawBitmap(note, 0, 0, null)
         note.recycle()

         val view = createBySize(viewSize, createBitmap)
         val viewBitmapSize = ZSize(view.getWidth, view.getHeight)
         val viewCanvas = new Canvas(view)
         viewCanvas.setMatrix(createMatrix(bitmapSize, 0, viewBitmapSize))
         viewCanvas.drawBitmap(withBackground, 0, 0, null)
         withBackground.recycle()

         val remoteViews = new RemoteViews(context.getApplicationContext().getPackageName(), R.layout.widget_blitznote)
         remoteViews.setImageViewBitmap(R.id.imgWidget, view)
         remoteViews.setOnClickPendingIntent(R.id.imgWidget, PendingIntent.getActivity(context, 0, new Intent(context, classOf[MainActivity]), 0))
         appWidgetManager.updateAppWidget(appWidgetId, remoteViews)
      }
   }

   private def createBySize[T](size: ZSize, creator: (ZSize) => T): T = {
      try {
         creator(size)
      }
      catch {
         case _: OutOfMemoryError =>
            val reducedSize = size.scale(0.5f, 0.5f)
            creator(reducedSize)
      }
   }
   private def createBitmap(size: ZSize) = Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888)
}
