package com.volidar.blitznote.client.android.common

import android.app.Activity
import android.widget.Toast
import android.view.Gravity

object DialogUtils {
   def alertNotImplemented(owner: Activity, featureName: String) {
      toast(owner, s"The feature '$featureName' is under construction.")
   }
   def alertNotImplemented(owner: Activity) {
      toast(owner, "This feature is under construction.")
   }
   private var lastToastOpt: Option[Toast] = None
   def toastLong(owner: Activity, message: String) = toastWithDuration(owner, message, Toast.LENGTH_LONG)
   def toast(owner: Activity, message: String) = toastWithDuration(owner, message, Toast.LENGTH_SHORT)
   private def toastWithDuration(owner: Activity, message: String, duration: Int) {
      if(owner == null) return
      owner.runOnUiThread(new Runnable{
         override def run() {
            for(obsoleteToast <- lastToastOpt) obsoleteToast.cancel()
            val newToast = Toast.makeText(owner, message, duration)
            newToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 0);
            newToast.show()
            lastToastOpt = Some(newToast)
         }
      })
   }
   def toastWithColor(owner: Activity, message: String, backgroundColor: Int) {
      if(owner == null) return
      owner.runOnUiThread(new Runnable{
         override def run() {
            for(obsoleteToast <- lastToastOpt) obsoleteToast.cancel()

            val newToast = Toast.makeText(owner, message, Toast.LENGTH_SHORT)
            newToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 0);
            newToast.getView.setBackgroundColor(backgroundColor)
            newToast.show()

            lastToastOpt = Some(newToast)
         }
      })
   }
}
