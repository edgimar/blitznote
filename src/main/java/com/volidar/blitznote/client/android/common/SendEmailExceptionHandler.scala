package com.volidar.blitznote.client.android.common

import java.lang.Thread.UncaughtExceptionHandler
import com.volidar.blitznote.client.android.{MainActivity, EmailHelper}
import java.io.{PrintWriter, StringWriter}

object ExceptionUtils {
   def getMessageAndStackTrace(ex: Throwable) = ex.toString + "\r\n" + getStackTrace(ex)

   def getStackTrace(ex: Throwable) = {
      val buffer = new StringWriter
      val printer = new PrintWriter(buffer)
      ex.printStackTrace(printer)
      printer.close()
      buffer.toString
   }
}

object SendEmailExceptionHandler {
   def applyToCurrentThread() = applyTo(Thread.currentThread())
   def applyTo(thread: Thread) = {
      thread.getUncaughtExceptionHandler match {
         case _: SendEmailExceptionHandler => ;//ignore, because already installed
         case oldHandler => thread.setUncaughtExceptionHandler(new SendEmailExceptionHandler(oldHandler))
      }
   }
}

class SendEmailExceptionHandler(oldHandler: UncaughtExceptionHandler) extends UncaughtExceptionHandler {
   override def uncaughtException(thread: Thread, ex: Throwable) = {
      MainActivity.instanceOpt.foreach(_.recycle())
      val sender = new Thread() {
         override def run() = EmailHelper.sendEmailToDeveloper(s"Exception: $ex", ExceptionUtils.getStackTrace(ex), null)
      }
      sender.start()
      sender.join()
      MainActivity.instanceOpt.foreach(_.finish())

      if(oldHandler != null) oldHandler.uncaughtException(thread, ex)
   }
}
