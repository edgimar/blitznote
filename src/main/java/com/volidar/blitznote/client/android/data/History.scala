package com.volidar.blitznote.client.android.data

import com.volidar.blitznote.client.android.common.Logging
import com.volidar.blitznote.client.android.data.base._
import android.util.{DisplayMetrics, Base64}
import java.io.{ByteArrayInputStream, ObjectInputStream}
import android.view.Surface

case class DrawParams(noteWidthToHeight: Float, dpmm: Float, eraserWidthFromOne: Float, pencilColors: List[Int] = DrawParams.paintColors) {
   def pencilWidthFromOne = eraserWidthFromOne / 60 //if eraser is 1.5cm, then pencil is 0.25mm
}

object DrawParams {
   val paintColors = List(0xFF200098, 0xFFC80000, 0xFF009820, 0xFF000000)
   val paintColorNames = List("Blue", "Red", "Green", "Black")
}

case class History private(params: DrawParams, actions: List[History.Action], redoStack: List[List[History.Action]]) {
   def add(action: History.Action, removeLast: Boolean = false): History = {
      val previousTransactions = if(removeLast) actions.drop(1) else actions
      val newTransactions = (action :: previousTransactions).foldLeft((0, Nil: List[History.Action])) {
         case (readyResult@(eraseAllCount, _), _) if eraseAllCount >= History.maxHistoryEraseAllCount => readyResult
         case ((eraseAllCount, itemsToKeepReversed), History.EraseAll) => (eraseAllCount + 1, History.EraseAll :: itemsToKeepReversed)
         case ((eraseAllCount, itemsToKeepReversed), action) => (eraseAllCount, action :: itemsToKeepReversed)
      }._2.reverse
      val newRedoStack = action match {
         case History.Draw(_, -1) => redoStack //don't delete redoStack if we got 2 finger click (or movement)
         case _ => Nil
      }
      History(params, newTransactions, newRedoStack)
   }
   def redo: History = {
      if(redoStack.isEmpty) {
         actions match {
            case Nil => this
            case History.Draw(_, -1) :: _ => History(params, actions.tail, Nil) //Erase was added during doubleClick -> remove it
            case _ => this
         }
      } else {
         actions match {
            case Nil => History(params, redoStack.head, redoStack.tail)
            case History.Draw(_, -1) :: _ => History(params, redoStack.head ::: actions.tail, redoStack.tail) //Erase was added during doubleClick -> remove it
            case _ => History(params, redoStack.head ::: actions, redoStack.tail)
         }
      }
   }
   def undoLast: History = {
      if(actions.isEmpty) {
         this
      } else {
         History(params, actions.tail, List(actions.head) :: redoStack)
      }
   }
   def undoToEraseAll: History = {
      var undoNext = true
      val (undid, kept) = actions.span {
         case History.EraseAll => //remove EraseAll to, but mark it as found in order to span on next element
            val result = undoNext
            undoNext = false
            result
         case _ => undoNext
      }
      new History(params, kept, undid :: redoStack)
   }
}

object History extends Logging {
   sealed trait Action
   case class Draw(path: ZPath, paintIndex: Int) extends Action
   case object EraseAll extends Action
   case class EraseSelection(selection: ZPath) extends Action
   case class MoveSelection(selection: ZPath, vector: ZPoint, scale: Float, rotation: Float) extends Action

   val maxHistoryEraseAllCount = 16
   def restoreHistory(storedHistory: String, metrics: DisplayMetrics, orientation: Int, viewSize: ZSize): History = {
      //if(true) return defaultHistory //TODO deletes history
      if(storedHistory.isEmpty) {
         createDefaultHistory(metrics, orientation, viewSize)
      }
      else {
         try {
            val bytes = Base64.decode(storedHistory, Base64.DEFAULT)
            val in = new ObjectInputStream(new ByteArrayInputStream(bytes))
            val historyBuffer = in.readObject().asInstanceOf[History]
            historyBuffer.actions.foreach(action => assert(action.isInstanceOf[Action])) //check types of subitems
            in.close()
            historyBuffer
         }
         catch {
            case ex: Exception =>
               logWarn("Cannot restore History", ex)
               createDefaultHistory(metrics, orientation, viewSize)
         }
      }
   }

   def createDefaultHistory(metrics: DisplayMetrics, orientation: Int, viewSize: ZSize): History = {
      val noteSize = if(orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180) viewSize else viewSize.swap
      val (xdpmm, ydpmm) = (metrics.xdpi / mmPerInch, metrics.ydpi / mmPerInch)
      val dpmm = math.sqrt((xdpmm * xdpmm + ydpmm * ydpmm) / 2).toFloat
      val defaultParams = DrawParams(noteSize.widthToHeight, dpmm, 15f * dpmm / noteSize.width)
      History(defaultParams, Nil, Nil)
   }
}
