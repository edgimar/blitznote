package com.volidar.blitznote.client.android.v10

import scala.util.Try
import android.appwidget.AppWidgetManager
import android.os.Bundle

object AppWidgetManagerV10 {
   private val getAppWidgetOptionsMethodOption = Try{Some(classOf[AppWidgetManager].getMethod("getAppWidgetOptions", classOf[Int]))}.getOrElse(None)

   def getAppWidgetOptions(appWidgetManager: AppWidgetManager, id: Integer): Bundle = {
      getAppWidgetOptionsMethodOption match {
         case None => Bundle.EMPTY
         case Some(getAppWidgetOptionsMethod) => Try{getAppWidgetOptionsMethod.invoke(appWidgetManager, id).asInstanceOf[Bundle]}.getOrElse(Bundle.EMPTY)
      }
   }
}
