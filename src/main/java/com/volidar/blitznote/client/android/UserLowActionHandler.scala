package com.volidar.blitznote.client.android

import rx.functions.Action1
import android.view.MotionEvent
import rx.subjects.PublishSubject
import com.volidar.blitznote.client.android.data.base._
import com.volidar.blitznote.client.android.v10.MotionEventV10

object UserLowActionHandler {
   sealed trait UserLowAction
   case object KeyDownBack extends UserLowAction
   case object KeyUpBack extends UserLowAction
   //case object KeyDownStylus extends UserLowAction
   //case object KeyUpStylus extends UserLowAction
   case class ActionDown(motionEvent: MotionEvent) extends UserLowAction
   case class ActionUp(motionEvent: MotionEvent) extends UserLowAction
   case class ActionMove(motionEvent: MotionEvent) extends UserLowAction
   case object ActionCancel extends UserLowAction

   sealed trait UserActionState
   case object Initial extends UserActionState
   case object Pencil extends UserActionState
   case object Eraser extends UserActionState
   case object Selecting extends UserActionState
   case object Finished extends UserActionState

   val maxClickDuration = 250L
   val maxClickMovementMm = 2f
   val minLongClickDuration = 1000L
   val maxDoubleClickDuration = 600L
   val maxDoubleClickMovementMm = 10f

   case class Finger private(fingerNumber: Int, id: Int, startTime: Long, startPoint: ZPoint, maxDistanceMm: Float, releaseTime: Long, lastPoint: ZPoint) {
      def this(fingerNumber: Int, id: Int, startTime: Long, startPoint: ZPoint) = this(fingerNumber, id, startTime, startPoint, 0f, Long.MaxValue, startPoint)

      def isReleased: Boolean = releaseTime != Long.MaxValue
      def isClick: Boolean = releaseTime - startTime < maxClickDuration && maxDistanceMm < maxClickMovementMm
      def isLongClick: Boolean = releaseTime - startTime > minLongClickDuration && maxDistanceMm < maxClickMovementMm
      def release(newReleaseTime: Long) = Finger(fingerNumber, id, startTime, startPoint, maxDistanceMm, newReleaseTime, lastPoint)
      def moveTo(newMaxDistanceMm: Float, newPoint: ZPoint): Finger = Finger(fingerNumber, id, startTime, startPoint, newMaxDistanceMm, releaseTime, newPoint)
   }
   case class Fingers(buttonState: Int, fingerMap: Map[Int, Finger]) {
      def this() = this(0, Map.empty)

      def activeCount = fingerMap.count(!_._2.isReleased)
      def actionUpDown(event: MotionEvent): Fingers = {
         //add new fingers
         val newButtonState = if(fingerMap.isEmpty) MotionEventV10.getButtonState(event) else buttonState
         val newFingerIndexes = for(idx <- 0 until event.getPointerCount if !fingerMap.contains(event.getPointerId(idx))) yield idx
         var nextFingerNumber = fingerMap.size
         val newFingerMapDelta = newFingerIndexes.map {
            case idx =>
               val newFingerNumber = nextFingerNumber
               nextFingerNumber += 1
               val id = event.getPointerId(idx)
               id -> new Finger(newFingerNumber, id, event.getEventTime, new ZPoint(event.getX(idx), event.getY(idx)))
         }
         val fingerMapWithNew = fingerMap ++ newFingerMapDelta
         //mark released fingers
         def releaseAndReturnFingerMap(releasedIdx: Int): Map[Int, Finger] = {
            val id = event.getPointerId(releasedIdx)
            fingerMapWithNew.get(id) match {
               case None => fingerMapWithNew //return unchanged map
               case Some(justReleased) => fingerMapWithNew.updated(id, justReleased.release(event.getEventTime))
            }
         }
         val fingerMapWithReleased = event.getAction & MotionEvent.ACTION_MASK match {
            case MotionEvent.ACTION_UP => releaseAndReturnFingerMap(0)
            case MotionEvent.ACTION_POINTER_UP => releaseAndReturnFingerMap(event.getActionIndex)
            case _ => fingerMapWithNew
         }
         Fingers(newButtonState, fingerMapWithReleased)
      }
   }
}

import com.volidar.blitznote.client.android.UserLowActionHandler._
import com.volidar.blitznote.client.android.UserActionHandler._

class UserLowActionHandler(userActions: PublishSubject[UserAction], dpmm: Float) extends Action1[UserLowAction] {
   var fingers = new Fingers()
   var state: UserActionState = Initial
   var previousClick: Option[Fingers] = None
   var backDownTime = Long.MaxValue

   override def call(action: UserLowAction) {
      action match {
         case ActionCancel =>
            userActions.onNext(Rollback)
            fingers = new Fingers
            previousClick = None
            state = Initial
         case KeyDownBack =>
            userActions.onNext(Rollback)
            state = Finished
         case KeyUpBack =>
            userActions.onNext(UndoByBackKey)
            state = Initial
         case ActionDown(event) =>
            val prevFingerCount = fingers.activeCount
            fingers = fingers.actionUpDown(event)
            if(state != Finished) {
               def moveSelectionStart() = {
                  val fingerStartPointByFingerNumber = fingers.fingerMap.map{ case (_, f) => (f.fingerNumber, f.startPoint)}.toMap
                  val pointingFinger = fingerStartPointByFingerNumber(0)
                  val directingFinger = if(fingerStartPointByFingerNumber.contains(1)) fingerStartPointByFingerNumber(1) else pointingFinger
                  userActions.onNext(MoveSelectionStart(pointingFinger, directingFinger))
               }
               (prevFingerCount, fingers.activeCount) match {
                  case (0, 1) => state = fingers.buttonState match {
                     case MotionEventV10.BUTTON_SECONDARY => Eraser
                     case MotionEventV10.BUTTON_TERTIARY =>
                        moveSelectionStart()
                        Selecting
                     case _ => Pencil
                  }
                  case (1, 2) if state == Pencil =>
                     userActions.onNext(Rollback)
                     state = Eraser
                  case (2, 3) if state == Eraser =>
                     userActions.onNext(Rollback)
                     moveSelectionStart()
                     state = Selecting
                  case _ =>
                     userActions.onNext(Rollback)
                     state = Finished
               }
               if(state == Eraser) userActions.onNext(Erasers(fingers.fingerMap.map(_._2).filter(!_.isReleased).map(_.lastPoint)))
            }
         case ActionMove(event) =>
            var fingerMap = fingers.fingerMap
            for(idx <- 0 until event.getPointerCount; finger <- fingerMap.get(event.getPointerId(idx))) {
               val newPoint = new ZPoint(event.getX(idx), event.getY(idx))
               val points: List[ZPoint] = finger.lastPoint ::
                  (for(h <- 0 until event.getHistorySize) yield new ZPoint(event.getHistoricalX(idx, h), event.getHistoricalY(idx, h))).toList :::
                  List(newPoint)
               val newMaxDistanceMm = points.sliding(2).foldLeft(finger.maxDistanceMm) {
                  case (curMaxDistanceMm, (from :: to :: Nil)) =>
                     if(state == Pencil) {
                        userActions.onNext(Move(from, to))
                     }
                     else if(state == Eraser) userActions.onNext(Erase(from, to))
                     math.max(curMaxDistanceMm, finger.startPoint.distance(to) / dpmm)
                  case _ => 0 //ignore
               }
               fingerMap += finger.id -> finger.moveTo(newMaxDistanceMm, newPoint)
            }
            state match {
               case Pencil => userActions.onNext(MoveFinished)
               case Eraser => userActions.onNext(Erasers(fingerMap.map(_._2).filter(!_.isReleased).map(_.lastPoint)))
               case Selecting =>
                  val fingerLastPointByFingerNumber = fingers.fingerMap.map{ case (_, f) => (f.fingerNumber, f.lastPoint)}.toMap
                  val pointingFinger = fingerLastPointByFingerNumber(0)
                  val directingFinger = if(fingerLastPointByFingerNumber.contains(1)) fingerLastPointByFingerNumber(1) else pointingFinger
                  userActions.onNext(MoveSelection(pointingFinger, directingFinger))
               case _ => ;//nothing
            }
            fingers = Fingers(fingers.buttonState, fingerMap)
         case ActionUp(event) =>
            if(state != Finished) {
               for(idx <- 0 until event.getPointerCount; finger <- fingers.fingerMap.get(event.getPointerId(idx))) {
                  val newPoint = new ZPoint(event.getX(idx), event.getY(idx))
                  if(state == Pencil) {
                     userActions.onNext(Move(finger.lastPoint, newPoint))
                  }
                  else if(state == Eraser) userActions.onNext(Erase(finger.lastPoint, newPoint))
               }
               if(state == Pencil) {
                  userActions.onNext(MoveFinished)
               }
               else if(state == Eraser) userActions.onNext(Erasers(Nil))//we will finish anyway now, so hide cursors
            }

            fingers = fingers.actionUpDown(event)
            if(fingers.activeCount != 0) {
               state = Finished
            } else {
               //detect long click
               if(fingers.fingerMap.size == 1 && fingers.fingerMap.head._2.isLongClick) {
                  userActions.onNext(Rollback)
                  userActions.onNext(OpenMenu)
                  previousClick = None
               } else {
                  def simpleClick() {
                     previousClick = Some(fingers)
                     userActions.onNext(Commit) //commit if it's first click (like dot)
                  }
                  val isClick = fingers.fingerMap.forall(_._2.isClick)
                  (isClick, previousClick) match {
                     case (false, _) => //if !click -> commit
                        userActions.onNext(Commit)
                        previousClick = None
                     case (_, None) => simpleClick()
                     case (_, Some(previousFingers)) =>
                        val isDoubleClickByTime = previousFingers.fingerMap.map(_._2.releaseTime).max - fingers.fingerMap.map(_._2.releaseTime).min < 500L
                        lazy val minDistance = fingers.fingerMap.foldLeft(Float.MaxValue) {
                           case (distance, (_, finger)) => math.min(distance, previousFingers.fingerMap.map(_._2.lastPoint.distance(finger.lastPoint)).min)
                        }
                        lazy val isDoubleClickByMinDistance = minDistance / dpmm < maxDoubleClickMovementMm
                        val nonDoubleClick = !(isDoubleClickByTime && isDoubleClickByMinDistance)
                        if(nonDoubleClick) {
                           simpleClick()
                        } else {
                           def sendDoubleClick(actions: UserAction*) {
                              actions.foreach(userActions.onNext)
                              previousClick = None
                           }
                           val (prevBtn, curBtn) = (previousFingers.buttonState, fingers.buttonState)
                           val (bPri, bSec, bTer) = (MotionEventV10.BUTTON_PRIMARY, MotionEventV10.BUTTON_SECONDARY, MotionEventV10.BUTTON_TERTIARY)
                           (previousFingers.fingerMap.size, fingers.fingerMap.size) match {
                              case (1, 1) if prevBtn == bSec && curBtn == bPri => sendDoubleClick(Undo, EraseAll)
                              case (1, 1) if prevBtn == bSec && curBtn == bSec => sendDoubleClick(Undo, SendEmail)
                              case (1, 1) if prevBtn == bPri && curBtn == bSec => sendDoubleClick(Undo, ChangePencil)
                              case (1, 1) if prevBtn == bPri && curBtn == bTer => sendDoubleClick(Undo, ChangePencilToEraser)
                              case (1, 1) if prevBtn == bSec && curBtn == bTer => sendDoubleClick(Redo)
                              case (1, 1) if prevBtn == bTer && curBtn == bPri => sendDoubleClick(UndoAll)
                              case (1, 1) if prevBtn == bTer && curBtn == bTer => sendDoubleClick(EraseSelection)
                              case (1, 1) if prevBtn == curBtn => simpleClick()

                              case (2, 1) if prevBtn == curBtn => sendDoubleClick(Undo, EraseAll)
                              case (2, 2) if prevBtn == curBtn => sendDoubleClick(Undo, SendEmail)
                              case (1, 2) if prevBtn == curBtn => sendDoubleClick(Undo, ChangePencil)
                              case (1, 3) if prevBtn == curBtn => sendDoubleClick(Undo, ChangePencilToEraser)
                              case (2, 3) if prevBtn == curBtn => sendDoubleClick(Redo)
                              case (3, 1) if prevBtn == curBtn => sendDoubleClick(UndoAll)
                              case (3, 3) if prevBtn == curBtn => sendDoubleClick(EraseSelection)
                              case _ => simpleClick()
                           }
                        }
                  }
               }
               fingers = new Fingers
               state = Initial
            }
      }
   }
}
