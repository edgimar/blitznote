package com.volidar.blitznote.client.android

import com.volidar.blitznote.client.android.common.DialogUtils
import android.app.Activity
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global

object TipManager {
   private val tips = List(
      "Exit - Home key",
      "Undo - Back key",
      "Erase - Draw with 2 Fingers",
      "Erase All - click: 2 Fingers, 1 Finger",
      "Send Email - double click: 2 Fingers",
      "Settings - long click",
      "Change Pencil - click: 1 Finger, 2 Fingers",
      "Select & Erase - Draw Selection & double click: 3 Fingers",
      "Select & Move - Draw Selection & move 3 Fingers",
      "Undo to Previous Note - click: 3 Fingers, 1 Finger"
   )

   def showTip(ownerActivity: Activity) {
      val currentTipIdx = scala.util.Random.nextInt(tips.size)
      val tip = tips(currentTipIdx)
      DialogUtils.toastLong(ownerActivity, tip)
   }
}
