import sbt._

import sbt.Keys._
import sbtandroid.AndroidPlugin._

object General {
   lazy val projectName = "AndroidClient"

   private lazy val settings = Defaults.defaultSettings ++ Seq(
      name := projectName,

      version := "00.05.01",
      versionCode := 10,
      platformName := "android-19",

      javaHome in Global := Some(file("C:\\Projects\\_Common\\jdk")),
      scalaHome in Global := Some(file("C:\\Projects\\_Common\\scala")),
      scalaVersion in Global := "2.10.3",

      javacOptions ++= Seq("-encoding", "UTF-8", "-source", "1.6", "-target", "1.6"), // Needed for android
      //TODO remove "-language:implicitConversions" after sbt-android 0.7.1
      scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation", "-feature", "-language:implicitConversions"), //, "-optimise"

      externalResolvers := {
         Seq(
            "android-sdk" at "file:///C:/Projects/_Common/android-sdk/extras/android/m2repository",
            "local-maven" at "file:///C:/Users/Zeus/.m2/repository"
            //"Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
         )
      },
      libraryDependencies ++= Seq(
         "org.scala-lang" % "scala-library" % "2.10.3",
         "dnsjava" % "dnsjava" % "2.1.6",
         "com.netflix.rxjava" % "rxjava-scala" % "0.17.6"
      )
      //Examples
      //libraryDependencies += apklib("com.actionbarsherlock" % "actionbarsherlock" % "4.3.1")
      //libraryDependencies += aarlib("com.google.android.gms" % "play-services" % "3.1.36")
   )

   private lazy val proguardSettings = Seq(
      useProguard := true,
      proguardOptimizations := Seq(
         //Proguard Examples: Scala
         "-keep class * implements org.xml.sax.EntityResolver",
         "-keepclassmembers class * { *** MODULE$; }",
         "-keepclassmembers class java.lang.Thread { *** parkBlocker; }",
         """-keepclassmembernames class scala.concurrent.forkjoin.ForkJoinPool {
           |   *** eventCount;
           |   *** workerCounts;
           |   *** runControl;
           |   *** syncStack;
           |   *** spareStack;
           |   *** ctl;
           |   *** stealCount;
           |   *** plock;
           |   *** indexSeed;
           |}""".stripMargin,
         "-keepclassmembernames class scala.concurrent.forkjoin.ForkJoinPool$WorkQueue { *** qlock; }",
         """-keepclassmembernames class scala.concurrent.forkjoin.ForkJoinWorkerThread {
           |   *** base;
           |   *** sp;
           |   *** runState;
           |}""".stripMargin,
         "-keepclassmembernames class scala.concurrent.forkjoin.ForkJoinTask { *** status; }",
         """-keepclassmembernames class scala.concurrent.forkjoin.LinkedTransferQueue {
           |   *** head;
           |   *** tail;
           |   *** cleanMe;
           |   *** sweepVotes;
           |}""".stripMargin,
         """-keepclassmembernames class scala.concurrent.forkjoin.LinkedTransferQueue$Node {
           |   *** item;
           |   *** next;
           |   *** waiter;
           |}""".stripMargin,
         //ENDOF: Proguard Examples: Scala

         //Proguard Examples: Android
         "-dontpreverify",
         "-repackageclasses ''",
         "-allowaccessmodification",
         //"-optimizations !code/simplification/arithmetic",//some arithmetic simplifications that Dalvik 1.0 and 1.5 can't handle => obsolete
         "-keepattributes *Annotation*",
         "-keep public class * extends android.app.Activity",
         "-keep public class * extends android.app.Application",
         "-keep public class * extends android.app.Service",
         "-keep public class * extends android.content.BroadcastReceiver",
         "-keep public class * extends android.content.ContentProvider",
         //"-keep public interface com.android.vending.licensing.ILicensingService",//enable if it will be used
         "-keepclasseswithmembers class * { public <init>(android.content.Context, android.util.AttributeSet); }",
         "-keepclasseswithmembers class * { public <init>(android.content.Context, android.util.AttributeSet, int); }",
         "-keepclassmembers class * implements android.os.Parcelable { static ** CREATOR; }",
         "-keepclassmembers class **.R$* { public static <fields>; }",
         "-keepclassmembers class * { @android.webkit.JavascriptInterface <methods>; }",
         """-keep public class * extends android.view.View {
           |    public <init>(android.content.Context);
           |    public <init>(android.content.Context, android.util.AttributeSet);
           |    public <init>(android.content.Context, android.util.AttributeSet, int);
           |    public void set*(...);
           |}""".stripMargin,
         """-keepclassmembers class * extends android.content.Context {
           |   public void *(android.view.View);
           |   public void *(android.view.MenuItem);
           |}""".stripMargin,
         //ENDOF: Proguard Examples: Android

         //Other making sense
         "-dontusemixedcaseclassnames",
         "-dontobfuscate",
         "-keepattributes Signature",
         "-keepattributes SourceFile,LineNumberTable,LocalVariable*",//TODO disable for release

         """-keep public class * extends android.view.View {
           |    public <init>(android.content.Context);
           |    public <init>(android.content.Context, android.util.AttributeSet);
           |    public <init>(android.content.Context, android.util.AttributeSet, int);
           |    public void set*(...);
           |}""".stripMargin,
         "-keepclassmembers class * extends android.app.Activity { public void *(android.view.View); }",
         "-keepclassmembers enum * { public static **[] values(); public static ** valueOf(java.lang.String); }",

         "-keep class javamail.** {*;}",
         "-keep class javax.mail.** {*;}",
         "-keep class javax.activation.** {*;}",
         "-keep class com.sun.mail.dsn.** {*;}",
         "-keep class com.sun.mail.handlers.** {*;}",
         "-keep class com.sun.mail.smtp.** {*;}",
         "-keep class com.sun.mail.util.** {*;}",
         "-keep class mailcap.** {*;}",
         "-keep class mimetypes.** {*;}",
         "-keep class myjava.awt.datatransfer.** {*;}",
         "-keep class org.apache.harmony.awt.** {*;}",
         "-keep class org.apache.harmony.misc.** {*;}",

         "-dontnote android.support.v4.text.ICUCompatIcs",
         "-dontnote org.xbill.DNS.**",
         //ENDOF: Other making sense

         /*Configurable*/
         "-dontoptimize",
         "-optimizations !**",
         //"-optimizationpasses 99",//TODO enable for release
         //"-optimizations class/*",
         //"-optimizations field/*",
         //"-optimizations method/marking/*,method/inlining/*,method/propagation/*",
         //"-optimizations code/merging,code/simplification/*,code/removal/*,code/allocation/variable",
         /*ENDOF: Configurable*/

         //Waiting
         "-dontskipnonpubliclibraryclasses",
         "-dontskipnonpubliclibraryclassmembers",
         "-verbose",
         //ENDOF: Waiting

         //Playground
         //"-keepclassmembers class scala.concurrent.ExecutionContext** { *; }",

         //"-dontwarn android.support.**",
         //"-dontwarn org.apache.harmony.awt.**",
         //"-dontwarn javax.activation.**",
         //"-dontwarn com.sun.mail.imap.protocol.**",

         //"-dontwarn scala.beans.**",
         //"-dontwarn scala.Enumeration$Val**",
         //"-dontwarn scala.concurrent.forkjoin.ForkJoinPool**",
         //ENDOF: Playground
         "")
   )

   lazy val fullAndroidSettings = {
      General.settings ++
         androidDefaults ++
         proguardSettings ++
         //Seq(
         //   logLevel in Global := Level.Debug,
         //   keyalias := "Volidar"
         //   //libraryDependencies += "org.scalatest" %% "scalatest" % "$scalatest_version$" % "test"
         //) ++
         Seq()//just to allow ++ at the end of all previous lines
   }
}

object AndroidBuild extends Build {
   lazy val main = Project(
      General.projectName,
      file("."),
      settings = General.fullAndroidSettings
   )

   //lazy val tests = Project(
   //   "tests",
   //   file("tests"),
   //   settings = General.settings ++
   //      AndroidTest.androidSettings ++
   //      General.proguardSettings ++ Seq(
   //      name := "$name$Tests"
   //   )
   //) dependsOn main
}
